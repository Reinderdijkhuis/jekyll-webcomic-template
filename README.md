# Jekyll webcomic template

A simple webcomic template for Jekyll which supports multiple comics on one site. 
The ruby script is what I use to generate [yncke.be](http://yncke.be). The rest of the files result in a ~~ugly~~ retro site and are merely meant as an illustration of how it can be used.

## Background

I had an old website of hand coded HTML pages which I wanted to update. After some searching, Jekyll looked like what I needed. After some more searching, I came across the [comical jekyll theme](https://github.com/chrisanthropic/comical-jekyll-theme) which was very eductional, but didn't fit my needs because I have more than one comic on the same site.

So I constructed a plugin based on a [datapage generator](https://github.com/avillafiorita/jekyll-datapage_gen) for this, which you can find here. 

## Disclaimer

I'm not a ruby developer. This project has been put online in the hope that it can be useful to someone. Please don't hold me responsible if it wastes your time, eats your cat, blows up your dinner or doesn't work at all.