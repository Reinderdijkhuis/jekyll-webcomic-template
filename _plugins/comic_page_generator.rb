# coding: utf-8
# Generate comic pages from individual records in yml files
# Copy pasted partss from data_page_generator from https://github.com/avillafiorita/jekyll-datapage_gen
# by Adolfo Villafiorita, which was distributed under the conditions of the MIT License, so I guess this
# is MIT License too.

module Jekyll

    # this class is used to tell Jekyll to generate a page
    class ComicPage < Page

     # - site and base are copied from other plugins: to be honest, I am not sure what they do
     def initialize(site, base, title, isenglish, category, index, pageurl, maxindex, hascover)
        @site = site
        @base = base
        pageindex = index + 1
        minindex = 1
        pagemaxindex = maxindex
        if hascover
            pageindex = index
            minindex = 0
            pagemaxindex = maxindex - 1
        end

        file_name = pageindex.to_s + ".html"
        file_path = "/html/"+ category + "/" + file_name

        self.process(file_name)
        self.read_yaml(File.join(base, '_layouts'), "comicpage.html")
        self.data['title'] = title
        self.data['isenglish'] = isenglish
        self.data['pagesrc'] = pageurl
        self.data['pagealt'] = title + " p. " + pageindex.to_s
        self.data['permalink'] = file_path
        if pageindex > 0
            self.data['pageindicator'] = pageindex.to_s + "/" + pagemaxindex.to_s
        end
        if pageindex > minindex
            self.data['first_in_category'] = "/html/" + category + "/" + minindex.to_s + ".html"
            previousindex = pageindex - 1
            self.data['previous_in_category'] = "/html/" + category + "/" + previousindex.to_s + ".html"
        end # if
        if pageindex < pagemaxindex
            nextindex = pageindex + 1
            self.data['next_in_category'] = "/html/" + category + "/" + nextindex.to_s + ".html"
        end # if
        self.data['last_in_category'] = "/html/" + category + "/" + pagemaxindex.to_s + ".html"
        # add all the information defined in _data for the current record to the
        # current page (so that we can access it with liquid tags)
        self.data.merge!(data)
     end # def
    end # class

   class ComicPagesGenerator < Generator
     safe true

     # generate loops over _config.yml/page_gen invoking the DataPage
     # constructor for each record for which we want to generate a page

     def generate(site)
         @site = site
         comics = site.config['comics_to_generate']
         if comics
            # The list of comic pages
             comics.each do |comic|
                 # The matching yml file for the comic pages
                 yml_file = comic['config_file']
                 yml_data = site.data[yml_file]
                 if yml_data
                    yml_data.each do |chapter|
                        title = chapter['title']
                        isenglish = chapter['isenglish']
                        category = chapter['category']
                        comic_pages = chapter['comic_pages']
                        lastindex = comic_pages.length
                        hascover = chapter['hascover']
                        comic_pages.each do |comic_page|
                            index = comic_pages.index comic_page
                            pageurl = comic_page['pageurl']
                            site.pages << ComicPage.new(site, site.source, title, isenglish, category, index, pageurl, lastindex, hascover)
                        end # comic_pages.each
                    end # yml_data.each
                 else
                     warn "The yml file " + yml_file + " does not exist."
                 end
              end # comics.each
         end # if comics
      end # generate(site)
    end # class ComicPagesGenerator

end # module

